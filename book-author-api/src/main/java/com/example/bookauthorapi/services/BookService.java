package com.example.bookauthorapi.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.bookauthorapi.models.Author;
import com.example.bookauthorapi.models.Book;

@Service
public class BookService {

    public List<Book> createBook(ArrayList<Author> authorList) {
        ArrayList<Book> bookList = new ArrayList<>();

        Book book1 = new Book("Pi of life", authorList, 10000, 3);
        Book book2 = new Book("Stockholme", authorList, 20000, 3);
        Book book3 = new Book("Petter pan", authorList, 40000);
        Book book4 = new Book("Aladin", authorList, 60000, 1);
        Book book5 = new Book("Changer", authorList, 60000, 2);

        bookList.addAll(Arrays.asList(book1, book2, book3, book4, book5));

        for (Book book : bookList) {
            System.out.println(book);
        }

        return bookList;
    }

    public List<Book> getBookListByQuantity(int qty, ArrayList<Book> bookList) {
        ArrayList<Book> filteredBookList = new ArrayList<>();
        for (Book book : bookList) {
            if (book.getQty() > qty) {
                filteredBookList.add(book);
            }
        }

        return filteredBookList;
    }
}
