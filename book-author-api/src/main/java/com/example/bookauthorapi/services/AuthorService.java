package com.example.bookauthorapi.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.bookauthorapi.models.Author;

@Service
public class AuthorService {
    public List<Author> createAuthor() {

        ArrayList<Author> authorList = new ArrayList<>();
        Author author1 = new Author("Tam", "tam@gmail.com", 'm');
        Author author2 = new Author("Minh", "minh@gmail.com", 'm');
        Author author3 = new Author("Trinh", "trinh@gmail.com", 'f');
        authorList.addAll(Arrays.asList(author1, author2, author3));
        for (Author author : authorList) {
            System.out.println(author);
        }
        return authorList;
    }

    public Author getAuthorInfo(String email, ArrayList<Author> authorList) {

        for (Author author : authorList) {
            if (author.getEmail().equals(email)) {
                return author;
            }
        }
        return null;
    }

    public Author getAuthorGender(Character gender, ArrayList<Author> authorList) {

        for (Author author : authorList) {
            if (author.getGender() == gender) {
                return author;
            }
        }
        return null;
    }
}
