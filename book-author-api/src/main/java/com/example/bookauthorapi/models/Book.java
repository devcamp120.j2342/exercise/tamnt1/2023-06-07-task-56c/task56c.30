package com.example.bookauthorapi.models;

import java.util.ArrayList;

public class Book {
    private String name;
    private ArrayList<Author> author;
    private double price;
    private int qty = 0;

    public Book(String name, ArrayList<Author> authorList, double price) {
        this.name = name;
        this.author = authorList;
        this.price = price;
    }

    public Book(String name, ArrayList<Author> author, double price, int qty) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qty = qty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Author> getAuthor() {
        return author;
    }

    public void setAuthor(ArrayList<Author> author) {
        this.author = author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void getAuthorName() {
        for (Author au : author) {
            System.out.println(au);
        }
    }

    @Override
    public String toString() {
        return "Book [name=" + name + ", author=" + author + ", price=" + price + ", qty=" + qty + "]";
    }

}
