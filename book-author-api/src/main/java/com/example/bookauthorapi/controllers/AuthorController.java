package com.example.bookauthorapi.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookauthorapi.models.Author;
import com.example.bookauthorapi.services.AuthorService;

@RestController
@CrossOrigin
@RequestMapping("/api")

public class AuthorController {
    @Autowired
    private AuthorService authorService;

    @GetMapping("/author-info")
    public Author getAuthorInformation(@RequestParam String email) {

        List<Author> authorList = authorService.createAuthor();
        return authorService.getAuthorInfo(email, (ArrayList<Author>) authorList);
    }

    @GetMapping("/author-gender")
    public Author getAuthorInformation(@RequestParam Character gender) {

        List<Author> authorList = authorService.createAuthor();
        return authorService.getAuthorGender(gender, (ArrayList<Author>) authorList);
    }
}
