package com.example.bookauthorapi.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookauthorapi.models.Author;
import com.example.bookauthorapi.models.Book;
import com.example.bookauthorapi.services.AuthorService;
import com.example.bookauthorapi.services.BookService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class BookController {
    @Autowired
    private BookService bookService;
    @Autowired
    private AuthorService authorService;

    @GetMapping("/book")
    public List<Book> getBook() {
        List<Author> authorList = authorService.createAuthor();

        return bookService.createBook((ArrayList<Author>) authorList);
    }

    @GetMapping("/book-quantity")
    public List<Book> getBookQty(@RequestParam int qty) {
        List<Author> authorList = authorService.createAuthor();

        List<Book> bookList = bookService.createBook((ArrayList<Author>) authorList);
        return bookService.getBookListByQuantity(qty, (ArrayList<Book>) bookList);

    }

}
